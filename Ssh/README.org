* Client Side
** ssh-keygen -t rsa -b 4096
** ssh-copy-id -i /path/to/the/generated/key user@host
* Server Side
** copy sshd_config in /etc/ssh
** set password authentication
** systemctl restart sshd
** copy key from client
** remove password authentication
** systemctl restart sshd
