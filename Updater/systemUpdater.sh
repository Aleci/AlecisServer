#!/bin/bash
while :
do
    apt update
    apt upgrade -y
    apt autoremove -y
    date > ~/lastSysUpdate.txt
    sleep 86400
done
