* Copy the systemUpdater.sh script into /bin
* Copy the updater.service definition into /etc/systemd/system
* Enable and start the service
** # systemctl enable updater.service
** # systemctl start updater.service
