#!/bin/bash

#============= CONFIGS =============

# Certificate details
EMAIL="aleci@inventati.org"
DOMAIN_NAME="trust.hack.casa"
PASSWORD="Alessandro"

# Ports used to perform the checks
# required to issue the certificate  
HTTP_PORT=8080
HTTPS_PORT=443

# ==================================

BASE_DIR="/etc/letsencrypt/live/$DOMAIN_NAME"
while :
do
    if openssl x509 -checkend 86400 -noout -in $BASE_DIR/fullchain.pem > /dev/null
    then
	date > ~/certbotLastCheck.txt
    else
	certbot certonly --apache --noninteractive --agree-tos --email $EMAIL -d $DOMAIN_NAME \
		--http-01-port $HTTP_PORT --tls-sni-01-port $HTTPS_PORT
	openssl pkcs12 -export -out cert.p12 -in $BASE_DIR/fullchain.pem -inkey $BASE_DIR/privkey.pem \
		-passout pass:$PASSWORD
	chown root:jellyfin $BASE_DIR/cert.p12
	chmod 750 $BASE_DIR/cert.p12
    fi
    sleep 86400
done

