* Get a domain name
* Set NS records pointing to the Hurricane Electric servers
- ns1.he.net -> ns5.he.net
* Get dns.he.net account
* Create records of interest and activate DynDNS passwords
** A records for myip
** TXT records for _acme-challenge
* Dynamical records commands
** curl "https://dyn.dns.he.net/nic/update" -d "hostname=MY-DOMAIN" -d "password=MY-DYN-DOMAIN-PASS" -d "myip=W.X.Y.Z"
** curl "https://dyn.dns.he.net/nic/update" -d "hostname=MY-DOMAIN" -d "password=MY-DYN-DOMAIN-PASS" -d "txt=MY-STRING"
* Certbot
* sudo certbot certonly --manual --preferred-challenges dns (--dry-run)
* sudo openssl pkcs12 -export -out cert.p12 -in $CERT_DIR/fullchain.pem -inkey $CERT_DIR/privkey.pem -passout pass:$PASSWORD
* curl http://checkip.dns.he.net/ | grep -E -o "[^^][0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}"
