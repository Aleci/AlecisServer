#!/bin/bash

#=== CONFIGS ==========================

DOMAIN=my-domain
PASSWORD=my-domain-password

#======================================

until eval " ping -c 3 www.google.com -q 2>&1" ; do
    sleep 15
done

while :
do
    CURRENT_IP_RAW=`curl http://checkip.dns.he.net/ | grep -E -o "[^^][0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}"`
    CURRENT_IP=`echo $CURRENT_IP_RAW`
    #
    CACHED_IP=`cat /root/cached_ip`
    #
    if [ $CURRENT_IP == $CACHED_IP ] ; then
    	echo - >> ~/ip_updates
    else
	echo $CURRENT_IP > /root/cached_ip
	curl "https://dyn.dns.he.net/nic/update" -d "hostname=$DOMAIN" -d "password=$PASSWORD" -d "myip=$CURRENT_IP"
	date > ~/ip_updates
    fi
    #
    sleep 1800
done
